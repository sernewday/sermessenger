<?php
define('SQL_SERVER',       'localhost');
define('SQL_USER',         'root');
define('SQL_PWD',          'root');
define('SQL_DB_NAME',      'db_messenger');
define('REG_LOGIN',        '([_,a-z,A-Z,0-9]{6,}$');
define('REG_PASSWORD',     '(?=[_a-zA-Z0-9]*?[A-Z])(?=[_a-zA-Z0-9]*?[a-z])(?=[_a-zA-Z0-9]*?[0-9])[_a-zA-Z0-9]{8,}');
define('REG_EMAIL',        '^([a-z,0-9,_]+)@([a-z,0-9,_,.]+)\.[a-z]{2,}$');
define('REG_NAME',         '[\s,_,а-я,А-Я,a-z,A-Z,0-9]{3,}$');
define('REG_AUTO_NUMBER',  '[A-Z,0-9]{7,}$');
define('REG_DECIMAL',      '^(?!-$)([0-9]{0,5}[.,]{0,2}[0-9]{0,2})$');