<?php
session_start();
include 'autoload.php';
$currentUrl = '/users';

if (!isset($_SESSION['logon'])) {
    header('Location: /login', true, 303);
    die;
}

$isExist = new IsExistsValidator();

$Users = new Users();
$rows = $Users->usconnect();
$count = round(count($rows) / 17 + 0.45);

$Groups = new Groups();
$select = $Groups->usconnect();

if (!empty($_GET['did'])) {
    $data = (object) null;
    $data->id = $_GET['did'];
    $data->query_type = 'del';
    if ($Users->save($data)) {
        echo 'OK';
        die;
    } else {
        header('Location: /dataError?info=Record not deleted!', true, 303);
    }
}

if (!empty($_GET['sid'])) {
    if ($data = $Users->getById($_GET['sid'])) {
        echo json_encode($data);
        die;
    } else {
        header('Location: /dataError?info=Select do not received!', true, 303);
    }
}

if (!empty($_GET['login'])) {
    $data = $isExist->validate($_GET['login'], 'users', 'login');
    if ($data) {
        echo json_encode($data);
        die;
    } else {
        header('Location: /dataError?info=Select do not received!', true, 303);
    }
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'add') {
    $data = $Users->before_save($_POST);
    if ($Users->save($data)) {
        header('Location: /users', true, 303);
    } else {
        header('Location: /dataError?info=Record not insert!', true, 303);
    }
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'edit') {
    $data = $Users->before_save($_POST);
    if ($Users->save($data)) {
        header('Location: /users', true, 303);
    } else {
        header('Location: /dataError?info=Record not update!', true, 303);
    }
}
include('templ/users/index.php');
