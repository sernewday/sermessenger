<?php
session_start();
include 'autoload.php';
$currentUrl = '/marks';

if (!isset($_SESSION['logon'])) {
    header('Location: /login', true, 303);
    die;
}

$isExist = new IsExistsValidator();
$Marks = new Marks();
$rows = $Marks->usconnect();
$count = round(count($rows) / 17 + 0.45);

if (!empty($_GET['did'])) {
    $data = (object) null;
    $data->id = $_GET['did'];
    $data->query_type = 'del';
    if ($Marks->save($data)) {
        echo 'OK';
        die;
    } else {
        header('Location: /dataError?info=Record not deleted!', true, 303);
    }
}

if (!empty($_GET['sid'])) {
    if ($data = $Marks->getById($_GET['sid'])) {
        echo json_encode($data);
        die;
    } else {
        header('Location: /dataError?info=Select do not received!', true, 303);
    }
}

if (!empty($_GET['name'])) {
    $data = $isExist->validate($_GET['name'], 'marks', 'name');
    if ($data) {
        echo json_encode($data);
        die;
    } else {
        header('Location: /dataError?info=Select do not received!', true, 303);
    }
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'add') {
    $data = $isExist->validate($_POST['name'], 'marks', 'name');
    if ($data == 'YES') {
    } else {
        $data = $Marks->before_save($_POST);
        if ($Marks->save($data)) {
            header('Location: /marks', true, 303);
        } else {
            header('Location: /dataError?info=Record not insert!', true, 303);
        }
    }
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'edit') {
    $data = $isExist->validate($_POST['name'], 'marks', 'name');
    if ($data == 'YES') {
    } else {
        $data = $Marks->before_save($_POST);
        if ($Marks->save($data)) {
            header('Location: /marks', true, 303);
        } else {
            header('Location: /dataError?info=Record not update!', true, 303);
        }
    }
}
include('templ/marks/index.php');
