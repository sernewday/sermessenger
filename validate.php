<?php
spl_autoload_register(function ($class_name) {
    include 'classes/' . $class_name . '.php';
});

$result = true;

$form = new Form();

$formData = $form->getFormData($data);

foreach ($formData as $key => $value) {
    $ClassValidator = $key . 'Validator';
    $listValidator = new $ClassValidator();
    $fieldResult = $listValidator->validate($value[0], $value[1]);
    if (!$fieldResult) $result = false;
}
echo json_encode($result);
