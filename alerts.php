<?php
session_start();
include 'autoload.php';
$currentUrl = '/alerts';

if (!isset($_SESSION['logon'])) {
	header( 'Location: /login', true, 303 ); 
    die;
    }

$Alerts = new Alerts();

if(isset($_GET['last_seen'])) {
    echo $Alerts->outstanding_notifications($_GET['last_seen']);
    die();
}

if(isset($_GET['mark_read'])) {
    echo $Alerts->mark_read($_GET['mark_read']);
    die();
}


    $rows = $Alerts->usconnect();
    $count = ceil(count($rows)/17);
    
    include('templ/alerts/index.php');

