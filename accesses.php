<?php
session_start();
include 'autoload.php';
$currentUrl = '/accesses';

$Statuses = new Statuses();
$alertOptions = $Statuses->usconnect();

$Accesses = new Accesses();
$whomTree = $Accesses->getWhomTree();

if (!empty($_POST)) {

    $validate = $Accesses->validate($_POST);
    if ($validate['success']) {

        switch ($_POST['right_type']) {

            case 'alert':

                if ($Accesses->save($_POST)) {
                    header('Location: /accesses', true, 303);
                } else {
                    header('Location: /dataError?info=Data do not saved!', true, 303);
                }

                break;
        }
    } else {
        $infos = '';
        foreach ($validate['error'] as $key => $param) {
            if ($infos == '') {
                $infos = $param[0];
            } else {
                $infos = $infos . '&info[]=' . $param[0];
            }
        }
        header('Location: /dataError?info[]=' . $infos, true, 303);
    }
}

if (!empty($_GET['group_id'])) {
$data = $Accesses->getGroupRights($_GET);
echo json_encode($data); die;
}

if (!empty($_GET['user_id'])) {
    $data = $Accesses->getUserRights($_GET);
    echo json_encode($data); die;
    }

include('templ/accesses/index.php');
