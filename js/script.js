function getXHR(url, dest = '#', async = true, callback = null) {
    var request = new XMLHttpRequest();
    request.open('GET', url, async);
    request.send(null);
    request.onload = function() {
        if (request.status == 200) {
            if (dest != '#') window.location = dest;
            if (!!callback) {
                callback(request.response);
            }
        } else {
            alert('Error ' + request.status + ': ' + request.statusText + ' Response: ' + request.response);
        }
    };
}

function showGroupForm(id = 0) {
    if (id == 0) {
        document.getElementById('query_type').value = "add";
        document.getElementsByClassName('groups')[0].style.display = "none";
    } else {
        data = getXHR(
            '/groups?sid=' + id,
            dest = '#',
            true,
            response => {
                let data = JSON.parse(response);
                document.getElementById('name').value = data.name;
            }
        );
        document.getElementById('query_type').value = "edit";
        document.getElementById('id').value = id;
        document.getElementById('id_disabled').value = id;
        document.getElementsByClassName('groups')[0].style.display = "none";
    }
    document.getElementsByClassName('groups_modal')[0].style.display = "block";
}

function showUserForm(id = 0) {
    if (id == 0) {
        document.getElementById('query_type').value = "add";
        document.getElementsByClassName('users')[0].style.display = "none";
        document.getElementById('group_id').value = '';
    } else {
        data = getXHR(
            '/users?sid=' + id,
            dest = '#',
            true,
            response => {
                let data = JSON.parse(response);
                document.getElementById('login').value = data.login;
                document.getElementById('password').value = data.password;
                document.getElementById('email').value = data.email;
                document.getElementById('op_' + data.group_id + '').selected = true;
            }
        );
        document.getElementById('query_type').value = "edit";
        document.getElementById('id').value = id;
        document.getElementById('id_disabled').value = id;
        document.getElementsByClassName('users')[0].style.display = "none";
    }
    document.getElementsByClassName('users_modal')[0].style.display = "block";
}

function eyePass() {
    if (document.getElementById('eye_pass').checked == true) {
        document.getElementById('eye_pass').title = 'Hide password';
        document.getElementById('password').type = 'text';
    } else {
        document.getElementById('eye_pass').title = 'Show password';
        document.getElementById('password').type = 'password';
    }
}

function showGroupForm(id = 0) {
    if (id == 0) {
        document.getElementById('query_type').value = "add";
        document.getElementsByClassName('groups')[0].style.display = "none";
    } else {
        data = getXHR(
            '/groups?sid=' + id,
            dest = '#',
            true,
            response => {
                let data = JSON.parse(response);
                document.getElementById('name').value = data.name;
            }
        );
        document.getElementById('query_type').value = "edit";
        document.getElementById('id').value = id;
        document.getElementById('id_disabled').value = id;
        document.getElementsByClassName('groups')[0].style.display = "none";
    }
    document.getElementsByClassName('groups_modal')[0].style.display = "block";
}

function ExistName(form, field = 0, id = 0, query_type = 0) {
    let base = form.baseURI.split('/');
    if (form[query_type].value == 'edit') {
        form.submit();
    } else {
        let name = field == 0 ? 'name' : form[field].name;
        data = getXHR(
            '/' + base[base.length - 1] + '?' + name + '=' + form[field].value,
            dest = '#',
            true,
            response => {
                let data = JSON.parse(response);
                let = errorMessage = field == 0 ? 'ERROR: "Form Data Lost - Try Again"' : 'ERROR: "' + form[field].value + ' Already Exists - Enter Another"';
                if (data == 'YES') {
                    document.getElementById(name).value = errorMessage;
                } else {
                    form.submit();
                }
            }
        );
    }
}

function showMarkForm(id = 0) {
    if (id == 0) {
        document.getElementById('query_type').value = "add";
        document.getElementsByClassName('marks')[0].style.display = "none";
    } else {
        data = getXHR(
            '/marks?sid=' + id,
            dest = '#',
            true,
            response => {
                let data = JSON.parse(response);
                document.getElementById('name').value = data.name;
            }
        );
        document.getElementById('query_type').value = "edit";
        document.getElementById('id').value = id;
        document.getElementById('id_disabled').value = id;
        document.getElementsByClassName('marks')[0].style.display = "none";
    }
    document.getElementsByClassName('marks_modal')[0].style.display = "block";
}

function showModelForm(id = 0) {
    if (id == 0) {
        document.getElementById('query_type').value = "add";
        document.getElementsByClassName('models')[0].style.display = "none";
        document.getElementById('mark_id').value = '';
    } else {
        data = getXHR(
            '/models?sid=' + id,
            dest = '#',
            true,
            response => {
                let data = JSON.parse(response);
                document.getElementById('name').value = data.name;
                document.getElementById('op_' + data.mark_id + '').selected = true;
            }
        );
        document.getElementById('query_type').value = "edit";
        document.getElementById('id').value = id;
        document.getElementById('id_disabled').value = id;
        document.getElementsByClassName('models')[0].style.display = "none";
    }
    document.getElementsByClassName('models_modal')[0].style.display = "block";
}

function showStatusForm(id = 0) {
    if (id == 0) {
        document.getElementById('query_type').value = "add";
        document.getElementsByClassName('statuses')[0].style.display = "none";
    } else {
        data = getXHR(
            '/statuses?sid=' + id,
            dest = '#',
            true,
            response => {
                let data = JSON.parse(response);
                document.getElementById('name').value = data.name;
            }
        );
        document.getElementById('query_type').value = "edit";
        document.getElementById('id').value = id;
        document.getElementById('id_disabled').value = id;
        document.getElementsByClassName('statuses')[0].style.display = "none";
    }
    document.getElementsByClassName('statuses_modal')[0].style.display = "block";
}

function showCarForm(id = 0) {
    if (id == 0) {
        let options = document.querySelectorAll('option[id^=op]');
        options.forEach(e => e.checked = false);
        let select = document.querySelectorAll('select');
        select.forEach(e => e.value = '');
        document.getElementById('model_id').disabled = 'disabled';
        let input = document.querySelectorAll('input');
        input.forEach(e => { if (e.value != 'E n t e r' && e.name != 'last_editor') e.value = '' });
        let inputnumber = document.querySelectorAll('input[name^=id]');
        inputnumber.forEach(e => e.value = 0);
        document.getElementById('query_type').value = "add";
        document.getElementsByClassName('cars')[0].style.display = "none";
    } else {
        data = getXHR(
            '/cars?sid=' + id,
            dest = '#',
            true,
            response => {
                let data = JSON.parse(response);
                document.getElementById('opmk_' + data.mark_id + '').selected = true;
                document.getElementById('opmd_' + data.model_id + '').selected = true;
                document.getElementById('ops_' + data.status_id + '').selected = true;
                document.getElementById('reg_number').value = data.reg_number;
                document.getElementById('rent_amount').value = data.rent_amount;
            }
        );
        document.getElementById('query_type').value = "edit";
        document.getElementById('id').value = id;
        document.getElementById('id_disabled').value = id;
        document.getElementsByClassName('cars')[0].style.display = "none";
        document.getElementById('model_id').removeAttribute('disabled');
    }
    document.getElementById('car_form').style.display = "block";
}

function onchangeMarkInCarForm(id) {
    data = getXHR(
        '/cars?mkid=' + id,
        dest = '#',
        true,
        response => {
            let data = JSON.parse(response);
            let select = document.getElementById('model_id');
            select.disabled = false;
            let options = document.querySelectorAll('option[id^=opmd_]');
            options.forEach(e => e.remove());

            for (var key in data) {
                let newOption = new Option(data[key].name, data[key].id);
                newOption.setAttribute('id', 'opmd_ ' + data[key].id);
                select.appendChild(newOption);
            }
        }
    );
}



function showAllAlert() {
    let alerts = document.querySelectorAll('.alerts>tbody>tr');
    if (document.getElementById('img_alerts').getAttribute('src') == '/images/add.png') {
        document.getElementById('img_alerts').setAttribute('src', '/images/minus.png');
        document.getElementById('img_alerts').setAttribute('title', 'Hide readed');
        alerts.forEach((aler) => {
            aler.style.display = 'table-row';
        });
    } else {
        document.getElementById('img_alerts').setAttribute('src', '/images/add.png');
        document.getElementById('img_alerts').setAttribute('title', 'Show all');
        alerts.forEach((aler) => {
            if (aler.classList.contains('alert-unread')) {
                aler.style.display = 'none';
            }
        });
    }

}

function closeForm(classname) {
    document.getElementsByClassName(classname + '_modal')[0].style.display = "none";
    document.getElementsByClassName(classname)[0].style.display = "block";
}

function clearForm() {
    let checkboxes = document.querySelectorAll('input[name^=owner_id]');
    checkboxes.forEach(e => e.checked = false);
    checkboxes = document.querySelectorAll('input[name^=right_object]');
    checkboxes.forEach(e => e.checked = false);
    checkboxes = document.querySelectorAll('input[name^=right_options_id]');
    checkboxes.forEach(e => e.checked = false);
    checkboxes = document.querySelectorAll('input[name^=right_type]');
    checkboxes.forEach(e => e.checked = false);
    let input = document.querySelectorAll('input');
    input.forEach(e => {
        if (e.value != 'E n t e r' && e.name != 'last_editor' && e.name != 'owner_id' &&
            e.name != 'right_object' && e.name != 'right_type' && e.name != 'right_options_id[]')
            e.value = ''
    });
    let inputnumber = document.querySelectorAll('input[name^=id]');
    inputnumber.forEach(e => e.value = 0);
}

function showGroupRights(id) {
    let chboxoptions = document.querySelectorAll('input[name^=right_options_id]');
    chboxoptions.forEach(e => e.checked = false);
    let checkedObject;
    let checkboxes = document.querySelectorAll('input[name^=right_object]');
    checkboxes.forEach(e => {
        if (e.checked == true) {
            checkedObject = e.value;
        }
    });
    let checkedType;
    checkboxes = document.querySelectorAll('input[name^=right_type]');
    checkboxes.forEach(e => {
        if (e.checked == true) {
            checkedType = e.value;
        }
    });

    let getparams = new Array();;
    switch (checkedObject) {
        case 'statuses':
            if (checkedType == 'alert') {
                getparams[0] = 'statuses';
                getparams[1] = 'alert';
            }
            break;
    }

    if (getparams.length > 0) {
        data = getXHR(
            '/accesses?object=' + getparams[0] + '&type=' + getparams[1] + '&group_id=' + id,
            dest = '#',
            true,
            response => {
                let data = JSON.parse(response);
                chboxoptions.forEach(e => { if (data.includes(e.value)) e.checked = true });
            }
        );
    }
}

function showUserRights(id) {
    let chboxoptions = document.querySelectorAll('input[name^=right_options_id]');
    chboxoptions.forEach(e => e.checked = false);
    let checkedObject;
    let checkboxes = document.querySelectorAll('input[name^=right_object]');
    checkboxes.forEach(e => {
        if (e.checked == true) {
            checkedObject = e.value;
        }
    });
    let checkedType;
    checkboxes = document.querySelectorAll('input[name^=right_type]');
    checkboxes.forEach(e => {
        if (e.checked == true) {
            checkedType = e.value;
        }
    });

    let getparams = new Array();;
    switch (checkedObject) {
        case 'statuses':
            if (checkedType == 'alert') {
                getparams[0] = 'statuses';
                getparams[1] = 'alert';
            }
            break;
    }

    if (getparams.length > 0) {
        data = getXHR(
            '/accesses?object=' + getparams[0] + '&type=' + getparams[1] + '&user_id=' + id,
            dest = '#',
            true,
            response => {
                let data = JSON.parse(response);
                chboxoptions.forEach(e => { if (data.includes(e.value)) e.checked = true });
            }
        );
    }
}

function checkboxValidate(form) {
    let checked = new Array();
    let count = 0;
    let checkboxes = document.querySelectorAll('input[name^=owner_id]');
    checkboxes.forEach(e => {
        if (e.checked == true) {
            checked[0] = true;
            count += 1;
        }
    });
    checkboxes = document.querySelectorAll('input[name^=right_object]');
    checkboxes.forEach(e => {
        if (e.checked == true) {
            checked[1] = true;
            count += 1;
        }
    });
    checkboxes = document.querySelectorAll('input[name^=right_type]');
    checkboxes.forEach(e => {
        if (e.checked == true) {
            checked[2] = true;
            count += 1;
        }
    });
    checkboxes = document.querySelectorAll('input[name^=right_options_id]');
    checkboxes.forEach(e => {
        if (e.checked == true) {
            checked[3] = true;
            count += 1;
        }
    });
    if (count < 4) {
        let validate_pop = document.getElementById('validate_massage')
        let validate_close = document.createElement('img');
        validate_close.classList.add('notification_close');
        validate_close.src = '/images/close.png';
        validate_close.onclick = (e) => {
            e.stopPropagation();
            validate_pop.remove();
        };
        validate_pop.appendChild(validate_close);
        if (!checked[0]) {
            let div = document.createElement('div');
            div.classList.add('child');
            let h1 = document.createElement('h1');
            h1.innerText = 'Check One Group Or User';
            div.appendChild(h1);
            validate_pop.appendChild(div);
        }
        if (!checked[1]) {
            let div = document.createElement('div');
            div.classList.add('child');
            let h1 = document.createElement('h1');
            h1.innerText = 'Check One Rights Object';
            div.appendChild(h1);
            validate_pop.appendChild(div);
        }
        if (!checked[2]) {
            let div = document.createElement('div');
            div.classList.add('child');
            let h1 = document.createElement('h1');
            h1.innerText = 'Check Rights Type To Check Object';
            div.appendChild(h1);
            validate_pop.appendChild(div);
        }
        if (!checked[3]) {
            let div = document.createElement('div');
            div.classList.add('child');
            let h1 = document.createElement('h1');
            h1.innerText = 'Check Few Rights Options';
            div.appendChild(h1);
            validate_pop.appendChild(div);
        }
    } else {
        form.submit();
    }
}

function checkNotifications() {
    let last_seen = localStorage.getItem('lastSeenNotification') || 0;
    getXHR(
        '/alerts?last_seen=' + last_seen,
        dest = '#',
        true,
        response => {
            let data = JSON.parse(response);
            let drawer = document.getElementById('notification_drawer');
            data.forEach(notification => {
                let notification_div = document.createElement('div');
                notification_div.classList.add('notification');

                let notification_title = document.createElement('div');
                notification_title.classList.add('notification_title');
                notification_title.onclick = () => {
                    notification_div.remove();
                    getXHR(
                        '/alerts?mark_read=' + notification.id,
                        dest = '/alerts',
                        true
                    )
                };

                let notification_hint = document.createElement('span');
                notification_hint.classList.add('notification_hint');
                notification_hint.innerText = notification.sender + ' ' + notification.date;
                notification_title.appendChild(notification_hint);


                let notification_title_text = document.createElement('span');
                notification_title_text.innerText = notification.header;
                notification_title.appendChild(notification_title_text);

                let notification_close = document.createElement('img');
                notification_close.classList.add('notification_close');
                notification_close.src = '/images/close.png';
                notification_close.onclick = (e) => {
                    e.stopPropagation();
                    notification_div.remove();
                };
                notification_title.appendChild(notification_close);

                notification_div.appendChild(notification_title);

                let notification_body = document.createElement('div');
                notification_body.classList.add('notification_body');
                notification_body.innerText = notification.body;
                notification_div.appendChild(notification_body);

                let notification_button = document.createElement('button');
                notification_button.classList.add('notification_button');
                notification_button.innerText = 'Принято';
                notification_button.type = 'button';
                notification_button.onclick = () => {
                    notification_div.remove();
                    getXHR(
                        '/alerts?mark_read=' + notification.id,
                        dest = '#',
                        true
                    )
                }
                notification_div.appendChild(notification_button);

                drawer.appendChild(notification_div);
                if (last_seen < notification.id) {
                    last_seen = notification.id;
                    localStorage.setItem('lastSeenNotification', last_seen);
                }

                setTimeout(() => {
                    notification_div.remove();
                }, 30000);
            })
        }
    );
}

setInterval(checkNotifications, 5000);