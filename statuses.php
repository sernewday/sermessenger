<?php
session_start();
include 'autoload.php';
$currentUrl = '/statuses';

if (!isset($_SESSION['logon'])) {
    header('Location: /login', true, 303);
    die;
}

$isExist = new IsExistsValidator();
$Statuses = new Statuses();
$rows = $Statuses->usconnect();
$count = round(count($rows) / 17 + 0.45);

if (!empty($_GET['did'])) {
    $data = (object) null;
    $data->id = $_GET['did'];
    $data->query_type = 'del';
    if ($Statuses->save($data)) {
        echo 'OK';
        die;
    } else {
        header('Location: /dataError?info=Record not deleted!', true, 303);
    }
}

if (!empty($_GET['sid'])) {
    if ($data = $Statuses->getById($_GET['sid'])) {
        echo json_encode($data);
        die;
    } else {
        header('Location: /dataError?info=Select do not received!', true, 303);
    }
}

if (!empty($_GET['name'])) {
    $data = $isExist->validate($_GET['name'], 'statuses', 'name');
    if ($data) {
        echo json_encode($data);
        die;
    } else {
        header('Location: /dataError?info=Select do not received!', true, 303);
    }
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'add') {
    $data = $isExist->validate($_POST['name'], 'groups', 'name');
    if ($data == 'YES') {
    } else {
        $data = $Statuses->before_save($_POST);
        if ($Statuses->save($data)) {
            header('Location: /statuses', true, 303);
        } else {
            header('Location: /dataError?info=Record not insert!', true, 303);
        }
    }
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'edit') {
    $data = $isExist->validate($_POST['name'], 'groups', 'name');
    if ($data == 'YES') {
    } else {
        $data = $Statuses->before_save($_POST);
        if ($Statuses->save($data)) {
            header('Location: /statuses', true, 303);
        } else {
            header('Location: /dataError?info=Record not update!', true, 303);
        }
    }
}
include('templ/statuses/index.php');
