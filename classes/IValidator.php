<?php
interface IValidator
{
 
  public function validate($data);
  
}