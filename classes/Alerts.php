<?php

class Alerts extends DbConnect
{

    public function outstanding_notifications($last_seen=0)
    {
        $notifications = $this->connect->query(
            'SELECT  * FROM `alerts` WHERE to_whom_id = '.$_SESSION['id'].
            ' AND UNIX_TIMESTAMP(date)+10000>UNIX_TIMESTAMP(CURRENT_TIME())'.
            ' AND id>'.$last_seen
        );
        if ($notifications)
        {

            $outstanding_notifications = $notifications->fetch_all(MYSQLI_ASSOC);
            return json_encode($outstanding_notifications);
        }
    }

    public function usconnect()
    {
        if ($gry = $this->connect->query('SELECT  * FROM `alerts` WHERE to_whom_id = '.$_SESSION['id'])) {

            $alerts = $gry->fetch_all(MYSQLI_ASSOC);

            return $alerts;
        }
    }

    public function mark_read($read_id)
    {
        $result = $this->connect->query('UPDATE `alerts` SET `read`=1 WHERE `id`='.$read_id);
        return $result?'OK':'Error';
    }

    public function calcAlerts()
    {
        if ($gry = $this->connect->query('SELECT count(`id`) as count_alerts FROM `alerts` WHERE to_whom_id = '.$_SESSION['id'].' AND `read` = 0')) {

            $calcAlerts = $gry->fetch_array(MYSQLI_ASSOC);

            return $calcAlerts;
        }
    }

}
