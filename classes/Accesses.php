<?php
const TABLE_GROUP_ACCESS_FILDS_ALIAS = array(
    'id' => 0, 'group_id' => 'Group', 'right_object' => 'Rights Object', 'right_type' => 'Right Type', 'right_options_id' => 'Rights Options'
);
const TABLE_USER_ACCESS_FILDS_ALIAS = array(
    'id' => 0, 'user_id' => 'User', 'right_object' => 'Rights Object', 'right_type' => 'Right Type', 'right_options_id' => 'Rights Options'
);
const FILDS_GROUP = '`group_id`, `right_object`, `right_type`, `right_options_id`';
const TABLE_USER_ACCESS_FILDS = '`user_id`, `right_object`, `right_type`, `right_options_id`';

class Accesses extends DbConnect
{
    public function getWhomTree()
    {
        if ($gry = $this->connect->query('SELECT g.`id` AS group_id, g.`name` AS groupname, 
        u.`id` AS user_id, `login` 
        FROM `groups` AS g 
        LEFT  JOIN  `users` AS u ON g.`id` = u.`group_id`
        WHERE u.id IS  NOT NULL 
        ORDER BY g.`id`')) {

            $whoms = $gry->fetch_all(MYSQLI_ASSOC);
            $whomsTree = array();
            foreach ($whoms as $whom) {
                if (!isset($whomsTree[$whom['group_id']])) {
                    $whomsTree[$whom['group_id']][] = $whom['groupname'];
                }
                $whomsTree[$whom['group_id']][] = [$whom['user_id'], $whom['login']];
            }
            return $whomsTree;
        }
    }

    public function getGroupRights($array)
    {
        if ($gry = $this->connect->query('SELECT `right_options_id` AS id
        FROM `group_access` 
        WHERE `group_id` = ' . $array['group_id'] . ' AND `right_object` = "' . $array['object'] . '" AND `right_type` = "' . $array['type'] . '"')) {

            $groupRights = $gry->fetch_all(MYSQLI_ASSOC);
            $groupRightsId = array();
            foreach ($groupRights as $id) {
                $groupRightsId[] = $id['id'];
            }
            return $groupRightsId;
        }
    }

    public function getUserRights($array)
    {
        if ($gry = $this->connect->query('SELECT `right_options_id` AS id
        FROM `user_access` 
        WHERE `user_id` = ' . $array['user_id'] . ' AND `right_object` = "' . $array['object'] . '" AND `right_type` = "' . $array['type'] . '"')) {

            $userRights = $gry->fetch_all(MYSQLI_ASSOC);
            $userRightsId = array();
            if (count($userRights) > 0) {
                foreach ($userRights as $id) {
                    $userRightsId[] = $id['id'];
                }
            } else {
                if ($gry = $this->connect->query('SELECT `right_options_id` AS id
                    FROM `group_access` 
                    WHERE `right_object` = "' . $array['object'] . '" AND `right_type` = "' . $array['type'] . '"
                    AND `group_id` = (SELECT `group_id` FROM `users` WHERE `id` = '.$array['user_id'].')')) {

                    $groupRights = $gry->fetch_all(MYSQLI_ASSOC);
                    $userRightsId = array();
                    foreach ($groupRights as $id) {
                        $userRightsId[] = $id['id'];
                    }
                }
            }
        }
        return $userRightsId;
    }


    public function save($data)
    {
        if (!empty($data['owner_id'])) {
            $owner = explode('.', $data['owner_id']);
            $data[$owner[0]] = $owner[1];

            $this->connect->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
            if (!empty($data['group_id'])) {
                $this->connect->query('DELETE FROM `group_access` 
            WHERE `group_id` = ' . $data['group_id'] . ' AND
            `right_object` = "' . $data['right_object'] . '" 
            AND `right_type` = "' . $data['right_type'] . '"');
                foreach ($data['right_options_id'] as $options) {
                    $this->connect->query('INSERT INTO `group_access` (' . FILDS_GROUP . ') 
            VALUES (' . $data['group_id'] . ', "' . $data['right_object'] . '", 
            "' . $data['right_type'] . '", ' . $options . ')');
                }
            }
            if (!empty($data['user_id'])) {
                $this->connect->query('DELETE FROM `user_access` 
            WHERE `user_id` = ' . $data['user_id'] . ' AND
            `right_object` = "' . $data['right_object'] . '" 
            AND `right_type` = "' . $data['right_type'] . '"');
                foreach ($data['right_options_id'] as $options) {
                    $this->connect->query('INSERT INTO `user_access` (' . TABLE_USER_ACCESS_FILDS . ') 
                VALUES (' . $data['user_id'] . ', "' . $data['right_object'] . '", 
                "' . $data['right_type'] . '", ' . $options . ')');
                }
            }
            $commit = $this->connect->commit();
            $this->connect->close();

            return $commit;
        } else  return false;
    }

    public function validate($array)
    {

        $result['success'] = true;
        $result['error'] = array();

        if (!empty($array['owner_id'])) {
            foreach (TABLE_GROUP_ACCESS_FILDS_ALIAS as $fieds => $alias) {
                if (!isset($array[$fieds]) && $fieds != 'id' && $fieds != 'group_id' && $fieds != 'user_id') {
                    $result['success'] = false;
                    $result['error'][]  = array('Not Select At Least One ' . $alias);
                }
            }
        }

        if (empty($array['owner_id'])) {
            $result['success'] = false;
            $result['error'][]  = array('Not Select At Least One Group or User');
        }

        return $result;
    }
}
