<?php

class Users extends DbConnect
{

    public function usconnect()
    {
        if ($gry = $this->connect->query('SELECT  `password`, `email`, `u`.`id` as id, `u`.`group_id` as group_id, `g`.`name` as groupname,
        `u`.`login` as login FROM `users`AS u LEFT JOIN `groups` as g ON `u`.`group_id` = `g`.`id`')) {
            $users = $gry->fetch_all(MYSQLI_ASSOC);

            return $users;
        }
    }

    public function getById($id)
    {
        if ($gry = $this->connect->query('SELECT * FROM `users` WHERE `id` = ' . $id)) {
            $users = $gry->fetch_array(MYSQLI_ASSOC);
            return $users;
        }
    }

    public function save($data)
    {

        switch ($data->query_type) {

            case 'del':
                $id = $data->id;
                if ($this->connect->query('DELETE FROM `users` WHERE `id` = ' . $id)) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'upd':
                $id = $data->id;
                $set = $data->set;  
                if ($this->connect->query('UPDATE `users` SET ' . $set . ' WHERE `id` = ' . $id)) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'ins':
                $filds = $data->filds;
                $values = $data->values;
                if ($this->connect->query('INSERT INTO `users` (' . $filds . ') VALUES (' . $values . ')')) {
                    return true;
                } else {
                    return false;
                }
                break;
        }
    }

    public function before_save($array)
    {

        switch ($array['query_type']) {

            case 'add':
                $data = (object) null;
                $filds = '';
                $values = '';

                foreach ($array as $key => $value) {
                    if ($key != 'id' && $key != 'query_type') {
                        if ($filds == '') {
                            $filds = '`' . $key . '`';
                        } else {
                            $filds = $filds . ', `' . $key . '`';
                        }
                        if ($values == '') {
                            $values = '"' . $value . '"';
                        } else {
                            $values = $values . ', "' . $value . '"';
                        }
                    }
                }

                $data->query_type = 'ins';
                $data->filds = $filds;
                $data->values = $values;

                return $data;
                break;

                case 'edit':
                    $data = (object) null;
                    $set = '';
    
                    foreach ($array as $key => $value) {
                        if ($key != 'id' && $key != 'query_type') {
                            if ($set == '') {
                                $set = '`' . $key . '`="'.$value.'"';
                            } else {
                                $set = $set . ', `' . $key . '`="'.$value.'"';
                            }
                        }
                    }

                    $data->id = $array['id'];
                    $data->query_type = 'upd';
                    $data->set = $set;
    
                    return $data;
                    break;
        }
    }
}
