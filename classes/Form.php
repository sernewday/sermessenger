<?php
class Form extends AbstractForm
{
    function __construct()   {
  }
  public function createForm($validateData)  {

    foreach ($validateData as $key => $data)
      $form[$key] = $data;

    return $form;
  }

  public function getFormData($validateData)  {
    $form = $this->createForm($validateData);
    $formdata = array();

    foreach ($form as $key => $value) :
      $formdata[$key] = array(0 => $value[$key], 1 => $_REQUEST[$key]);
    endforeach;

    return $formdata;
  }
}
