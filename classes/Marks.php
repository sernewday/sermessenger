<?php

class Marks extends DbConnect
{

    public function usconnect()
    {
        if ($gry = $this->connect->query('SELECT * FROM `marks`')) {
            $marks = $gry->fetch_all(MYSQLI_ASSOC);

            return $marks;
        }
    }

    public function getById($id)
    {
        if ($gry = $this->connect->query('SELECT * FROM `marks` WHERE `id` = ' . $id)) {
            $marks = $gry->fetch_array(MYSQLI_ASSOC);
            return $marks;
        }
    }

    public function getChildsArray($id)
    {
        if ($gry = $this->connect->query('SELECT `id`, `name` FROM `models` 
        WHERE `mark_id` = ' . $id . ' 
        ORDER BY `name`')) {
            $childs = $gry->fetch_all(MYSQLI_ASSOC);

            return $childs;
        }
    }

    public function hasChilds()
    {
        if ($gry = $this->connect->query('SELECT DISTINCT  mk.`id` as id, mk.`name` as name   
        FROM `models` as md LEFT JOIN `marks` as mk ON mk.`id` = md.`mark_id`
        ORDER BY mk.`name`')) {
            $childs = $gry->fetch_all(MYSQLI_ASSOC);
            return $childs;
        }
    }


    public function save($data)
    {

        switch ($data->query_type) {

            case 'del':
                $id = $data->id;
                if ($this->connect->query('DELETE FROM `marks` WHERE `id` = ' . $id)) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'upd':
                $id = $data->id;
                $set = $data->set;
                if ($this->connect->query('UPDATE `marks` SET ' . $set . ' WHERE `id` = ' . $id)) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'ins':
                $filds = $data->filds;
                $values = $data->values;
                if ($this->connect->query('INSERT INTO `marks` (' . $filds . ') VALUES (' . $values . ')')) {
                    return true;
                } else {
                    return false;
                }
                break;
        }
    }

    public function before_save($array)
    {

        switch ($array['query_type']) {

            case 'add':
                $data = (object) null;
                $filds = '';
                $values = '';

                foreach ($array as $key => $value) {
                    if ($key != 'id' && $key != 'query_type') {
                        if ($filds == '') {
                            $filds = '`' . $key . '`';
                        } else {
                            $filds = $filds . ', `' . $key . '`';
                        }
                        if ($values == '') {
                            $values = '"' . $value . '"';
                        } else {
                            $values = $values . ', "' . $value . '"';
                        }
                    }
                }

                $data->query_type = 'ins';
                $data->filds = $filds;
                $data->values = $values;

                return $data;
                break;

            case 'edit':
                $data = (object) null;
                $set = '';

                foreach ($array as $key => $value) {
                    if ($key != 'id' && $key != 'query_type') {
                        if ($set == '') {
                            $set = '`' . $key . '`="' . $value . '"';
                        } else {
                            $set = $set . ', `' . $key . '`="' . $value . '"';
                        }
                    }
                }

                $data->id = $array['id'];
                $data->query_type = 'upd';
                $data->set = $set;

                return $data;
                break;
        }
    }
}
