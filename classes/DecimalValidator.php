<?php
class DecimalValidator implements IValidator

{
  function __construct()
  {
  }
  //defalt dicimal format 7,2
  public function validate($data, $condition = '')
  {
    if ($condition == '') $condition = "/^(?!\-$)(?!0$)([0-9]{0,5}[.,]{0,2}[0-9]{0,2})$/";
    if (!preg_match($condition, $data)) {
      return true;
    } else return false;
  }
}
