<?php

class Cars extends DbConnect
{

    public function usconnect()
    {
        if ($gry = $this->connect->query('SELECT  `c`.`id` as id, `md`.`mark_id` as mark_id, `mk`.`name` as markname,
                                               `md`.`id` as model_id, `md`.`name` as modelname, `c`.`reg_number` as reg_number,
                                                `c`.`rent_amount` as rent_amount, `s`.`id` as status_id, `s`.`name` as statusname
                                            FROM `cars`AS c 
                                            LEFT JOIN `marks` as mk ON `c`.`mark_id` = `mk`.`id`			
                                            LEFT JOIN `models` as md ON `c`.`model_id` = `md`.`id`
                                            LEFT JOIN `statuses` as s ON `c`.`status_id` = `s`.`id`')) {

            $cars = $gry->fetch_all(MYSQLI_ASSOC);

            return $cars;
        }
    }

    public function getById($id)
    {
        if ($gry = $this->connect->query('SELECT * FROM `cars` WHERE `id` = ' . $id)) {
            $cars = $gry->fetch_array(MYSQLI_ASSOC);
            return $cars;
        }
    }

    public function save($data)
    {

        switch ($data->query_type) {

            case 'del':
                $id = $data->id;
                if ($this->connect->query('DELETE FROM `cars` WHERE `id` = ' . $id)) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'upd':
                $id = $data->id;
                $set = $data->set;
                if ($this->connect->query('UPDATE `cars` SET ' . $set . ' WHERE `id` = ' . $id)) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'ins':
                $filds = $data->filds;
                $values = $data->values;
                if ($this->connect->query('INSERT INTO `cars` (' . $filds . ') VALUES (' . $values . ')')) {
                    return true;
                } else {
                    return false;
                }
                break;
        }
    }

    public function before_save($array)
    {

        switch ($array['query_type']) {

            case 'add':
                $data = (object) null;
                $filds = '';
                $values = '';

                foreach ($array as $key => $value) {
                    if ($key != 'id' && $key != 'query_type') {
                        if ($filds == '') {
                            $filds = '`' . $key . '`';
                        } else {
                            $filds = $filds . ', `' . $key . '`';
                        }
                        if ($values == '') {
                            $values = '"' . $value . '"';
                        } else {
                            $values = $values . ', "' . $value . '"';
                        }
                    }
                }

                $data->query_type = 'ins';
                $data->filds = $filds;
                $data->values = $values;

                return $data;
                break;

            case 'edit':
                $data = (object) null;
                $set = '';

                foreach ($array as $key => $value) {
                    if ($key != 'id' && $key != 'query_type') {
                        if ($set == '') {
                            $set = '`' . $key . '`="' . $value . '"';
                        } else {
                            $set = $set . ', `' . $key . '`="' . $value . '"';
                        }
                    }
                }

                $data->id = $array['id'];
                $data->query_type = 'upd';
                $data->set = $set;

                return $data;
                break;
        }
    }
}
