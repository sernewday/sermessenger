<?php
include "config.php";

class DbConnect {

    public $connect = null;
    
    function  __construct() {
        $this->connect = mysqli_connect(SQL_SERVER, SQL_USER, SQL_PWD, SQL_DB_NAME);
        if ( mysqli_connect_errno() ) {
            exit('Failed to connect to MySQL: ' . mysqli_connect_error());
        }   
    }
    
}
    