<?php

class Models extends DbConnect
{

    public function usconnect()
    {
        if ($gry = $this->connect->query('SELECT  `md`.`id` as id, `md`.`mark_id` as mark_id, `mk`.`name` as markname,
             `md`.`name` as name FROM `models`AS md LEFT JOIN `marks` as mk ON `md`.`mark_id` = `mk`.`id`')) {
            $models = $gry->fetch_all(MYSQLI_ASSOC);
            
            return $models;
        }
    }

    public function getById($id)
    {
        if ($gry = $this->connect->query('SELECT * FROM `models` WHERE `id` = ' . $id)) {
            $models = $gry->fetch_array(MYSQLI_ASSOC);
            return $models;
        }
    }

    public function save($data)
    {

        switch ($data->query_type) {

            case 'del':
                $id = $data->id;
                if ($this->connect->query('DELETE FROM `models` WHERE `id` = ' . $id)) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'upd':
                $id = $data->id;
                $set = $data->set;  
                if ($this->connect->query('UPDATE `models` SET ' . $set . ' WHERE `id` = ' . $id)) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'ins':
                $filds = $data->filds;
                $values = $data->values;
                if ($this->connect->query('INSERT INTO `models` (' . $filds . ') VALUES (' . $values . ')')) {
                    return true;
                } else {
                    return false;
                }
                break;
        }
    }

    public function before_save($array)
    {

        switch ($array['query_type']) {

            case 'add':
                $data = (object) null;
                $filds = '';
                $values = '';

                foreach ($array as $key => $value) {
                    if ($key != 'id' && $key != 'query_type') {
                        if ($filds == '') {
                            $filds = '`' . $key . '`';
                        } else {
                            $filds = $filds . ', `' . $key . '`';
                        }
                        if ($values == '') {
                            $values = '"' . $value . '"';
                        } else {
                            $values = $values . ', "' . $value . '"';
                        }
                    }
                }

                $data->query_type = 'ins';
                $data->filds = $filds;
                $data->values = $values;

                return $data;
                break;

                case 'edit':
                    $data = (object) null;
                    $set = '';
    
                    foreach ($array as $key => $value) {
                        if ($key != 'id' && $key != 'query_type') {
                            if ($set == '') {
                                $set = '`' . $key . '`="'.$value.'"';
                            } else {
                                $set = $set . ', `' . $key . '`="'.$value.'"';
                            }
                        }
                    }

                    $data->id = $array['id'];
                    $data->query_type = 'upd';
                    $data->set = $set;
    
                    return $data;
                    break;
        }
    }
}
