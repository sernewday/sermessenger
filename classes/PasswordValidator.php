<?php
class PasswordValidator implements  IValidator

{
    function __construct() {
      }

      public function validate($data, $condition = ''){
        if ($condition=='') $condition = '/' . REG_PASSWORD . '/';
          if (preg_match($condition, $data) || strlen($data)<=7) {
            return false;
          }
          else return true;
      }
}