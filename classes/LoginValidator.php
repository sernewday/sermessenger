<?php
class LoginValidator implements IValidator

{
  function __construct()
  {
  }

  public function validate($data, $condition = '')
  {
    if ($condition == '') $condition = '/' . REG_LOGIN . '/';
    if (!preg_match($condition, $data)) {
      return true;
    } else return false;
  }
}
