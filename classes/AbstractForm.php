<?php
abstract class AbstractForm {

   public $validateData = array();

    abstract public function createForm($validateData);

    abstract public function getFormData($validateData);          
}