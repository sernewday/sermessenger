<?php
class AutoNumberValidator implements  IValidator

{
    function __construct() {
      }

      public function validate($data, $condition = ''){
        if ($condition=='') $condition = '/' . REG_AUTO_NUMBER . '/';
          if (preg_match($condition, $data) || strlen($data)<=7) {
            return false;
          }
          else return true;
      }
}