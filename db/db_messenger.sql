-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.47 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5958
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_messenger
CREATE DATABASE IF NOT EXISTS `db_messenger` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `db_messenger`;

-- Dumping structure for table db_messenger.alerts
CREATE TABLE IF NOT EXISTS `alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `to_whom_id` int(11) NOT NULL DEFAULT '0',
  `sender` varchar(50) NOT NULL DEFAULT ' ',
  `header` varchar(50) NOT NULL DEFAULT ' ',
  `body` varchar(50) NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;

-- Dumping data for table db_messenger.alerts: ~39 rows (approximately)
/*!40000 ALTER TABLE `alerts` DISABLE KEYS */;
REPLACE INTO `alerts` (`id`, `date`, `to_whom_id`, `sender`, `header`, `body`, `read`) VALUES
	(47, '2020-10-07 17:18:57', 5, 'Cowboy5', 'TC 22345SX', 'Статус изменился на Резерв', 0),
	(48, '2020-10-07 17:18:57', 11, 'Cowboy5', 'TC 22345SX', 'Статус изменился на Резерв', 0),
	(49, '2020-10-07 17:18:57', 12, 'Cowboy5', 'TC 22345SX', 'Статус изменился на Резерв', 0),
	(50, '2020-10-07 17:18:57', 18, 'Cowboy5', 'TC 22345SX', 'Статус изменился на Резерв', 0),
	(51, '2020-10-07 17:18:57', 26, 'Cowboy5', 'TC 22345SX', 'Статус изменился на Резерв', 0),
	(54, '2020-10-07 17:19:11', 11, 'Cowboy5', 'TC AE3776AC', 'Статус изменился на В работе', 0),
	(55, '2020-10-07 17:19:11', 26, 'Cowboy5', 'TC AE3776AC', 'Статус изменился на В работе', 0),
	(57, '2020-10-07 17:19:19', 5, 'Cowboy5', 'TC AE3586BE', 'Статус изменился на На выдачу', 0),
	(58, '2020-10-07 17:19:19', 11, 'Cowboy5', 'TC AE3586BE', 'Статус изменился на На выдачу', 0),
	(59, '2020-10-07 17:19:19', 12, 'Cowboy5', 'TC AE3586BE', 'Статус изменился на На выдачу', 0),
	(60, '2020-10-07 17:19:19', 18, 'Cowboy5', 'TC AE3586BE', 'Статус изменился на На выдачу', 0),
	(61, '2020-10-07 17:19:19', 26, 'Cowboy5', 'TC AE3586BE', 'Статус изменился на На выдачу', 0),
	(62, '2020-10-07 17:19:19', 28, 'Cowboy5', 'TC AE3586BE', 'Статус изменился на На выдачу', 0),
	(64, '2020-10-07 17:19:26', 5, 'Cowboy5', 'TC 12445SX', 'Статус изменился на На выдачу', 0),
	(65, '2020-10-07 17:19:26', 11, 'Cowboy5', 'TC 12445SX', 'Статус изменился на На выдачу', 0),
	(66, '2020-10-07 17:19:26', 12, 'Cowboy5', 'TC 12445SX', 'Статус изменился на На выдачу', 0),
	(67, '2020-10-07 17:19:26', 18, 'Cowboy5', 'TC 12445SX', 'Статус изменился на На выдачу', 0),
	(68, '2020-10-07 17:19:26', 26, 'Cowboy5', 'TC 12445SX', 'Статус изменился на На выдачу', 0),
	(69, '2020-10-07 17:19:26', 28, 'Cowboy5', 'TC 12445SX', 'Статус изменился на На выдачу', 0),
	(71, '2020-10-07 17:41:05', 11, 'Cowboy5', 'TC 12345SN', 'Статус изменился на В работе', 0),
	(72, '2020-10-07 17:41:05', 26, 'Cowboy5', 'TC 12345SN', 'Статус изменился на В работе', 0),
	(74, '2020-10-07 17:41:11', 5, 'Cowboy5', 'TC BI1234DH', 'Статус изменился на На выдачу', 0),
	(75, '2020-10-07 17:41:11', 11, 'Cowboy5', 'TC BI1234DH', 'Статус изменился на На выдачу', 0),
	(76, '2020-10-07 17:41:11', 12, 'Cowboy5', 'TC BI1234DH', 'Статус изменился на На выдачу', 0),
	(77, '2020-10-07 17:41:11', 18, 'Cowboy5', 'TC BI1234DH', 'Статус изменился на На выдачу', 0),
	(78, '2020-10-07 17:41:11', 26, 'Cowboy5', 'TC BI1234DH', 'Статус изменился на На выдачу', 0),
	(79, '2020-10-07 17:41:11', 28, 'Cowboy5', 'TC BI1234DH', 'Статус изменился на На выдачу', 0),
	(81, '2020-10-07 17:41:23', 5, 'Cowboy5', 'TC AE3776AC', 'Статус изменился на На выдачу', 0),
	(82, '2020-10-07 17:41:23', 11, 'Cowboy5', 'TC AE3776AC', 'Статус изменился на На выдачу', 0),
	(83, '2020-10-07 17:41:23', 12, 'Cowboy5', 'TC AE3776AC', 'Статус изменился на На выдачу', 0),
	(84, '2020-10-07 17:41:23', 18, 'Cowboy5', 'TC AE3776AC', 'Статус изменился на На выдачу', 0),
	(85, '2020-10-07 17:41:23', 26, 'Cowboy5', 'TC AE3776AC', 'Статус изменился на На выдачу', 0),
	(86, '2020-10-07 17:41:23', 28, 'Cowboy5', 'TC AE3776AC', 'Статус изменился на На выдачу', 0),
	(88, '2020-10-07 17:41:28', 5, 'Cowboy5', 'TC AH3634DH', 'Статус изменился на На выдачу', 0),
	(89, '2020-10-07 17:41:28', 11, 'Cowboy5', 'TC AH3634DH', 'Статус изменился на На выдачу', 0),
	(90, '2020-10-07 17:41:28', 12, 'Cowboy5', 'TC AH3634DH', 'Статус изменился на На выдачу', 0),
	(91, '2020-10-07 17:41:28', 18, 'Cowboy5', 'TC AH3634DH', 'Статус изменился на На выдачу', 0),
	(92, '2020-10-07 17:41:28', 26, 'Cowboy5', 'TC AH3634DH', 'Статус изменился на На выдачу', 0),
	(93, '2020-10-07 17:41:28', 28, 'Cowboy5', 'TC AH3634DH', 'Статус изменился на На выдачу', 0);
/*!40000 ALTER TABLE `alerts` ENABLE KEYS */;

-- Dumping structure for table db_messenger.cars
CREATE TABLE IF NOT EXISTS `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mark_id` varchar(50) NOT NULL,
  `model_id` varchar(50) NOT NULL,
  `reg_number` varchar(50) NOT NULL,
  `rent_amount` decimal(7,2) DEFAULT '0.00',
  `status_id` varchar(50) NOT NULL,
  `last_editor` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table db_messenger.cars: ~7 rows (approximately)
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
REPLACE INTO `cars` (`id`, `mark_id`, `model_id`, `reg_number`, `rent_amount`, `status_id`, `last_editor`) VALUES
	(1, '1', '7', '22345SX', 50.00, '2', 11),
	(2, '3', '9', '12345SN', 232.24, '4', 11),
	(3, '5', '11', '12445SX', 234.88, '1', 11),
	(4, '2', '8', '12665SX', 234.88, '1', 11),
	(6, '11', '14', 'AH3634DH', 200.00, '1', 11),
	(7, '6', '12', 'AE3776AC', 44.50, '1', 11),
	(8, '7', '24', 'BI1234DH', 150.12, '1', 11),
	(9, '8', '22', 'AE3586BE', 400.00, '1', 11);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;

-- Dumping structure for table db_messenger.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table db_messenger.groups: ~6 rows (approximately)
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
REPLACE INTO `groups` (`id`, `name`) VALUES
	(2, 'Platinum'),
	(3, 'Gold'),
	(4, 'Silver'),
	(5, 'Special'),
	(6, 'Lux'),
	(10, 'Admin');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;

-- Dumping structure for table db_messenger.group_access
CREATE TABLE IF NOT EXISTS `group_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `right_object` varchar(50) NOT NULL DEFAULT '',
  `right_type` varchar(50) NOT NULL DEFAULT '',
  `right_options_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table db_messenger.group_access: ~9 rows (approximately)
/*!40000 ALTER TABLE `group_access` DISABLE KEYS */;
REPLACE INTO `group_access` (`id`, `group_id`, `right_object`, `right_type`, `right_options_id`) VALUES
	(69, 10, 'statuses', 'alert', 1),
	(70, 10, 'statuses', 'alert', 2),
	(71, 10, 'statuses', 'alert', 3),
	(72, 10, 'statuses', 'alert', 4),
	(73, 10, 'statuses', 'alert', 5),
	(80, 3, 'statuses', 'alert', 1),
	(81, 5, 'statuses', 'alert', 1),
	(82, 5, 'statuses', 'alert', 2),
	(83, 5, 'statuses', 'alert', 3),
	(84, 5, 'statuses', 'alert', 4),
	(85, 5, 'statuses', 'alert', 5);
/*!40000 ALTER TABLE `group_access` ENABLE KEYS */;

-- Dumping structure for table db_messenger.marks
CREATE TABLE IF NOT EXISTS `marks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table db_messenger.marks: ~10 rows (approximately)
/*!40000 ALTER TABLE `marks` DISABLE KEYS */;
REPLACE INTO `marks` (`id`, `name`) VALUES
	(1, 'Volkswagen'),
	(2, 'Skoda'),
	(3, 'Toyota'),
	(4, 'Honda'),
	(5, 'Peugeot'),
	(6, 'Hyundai'),
	(7, 'Dodge'),
	(8, 'Chrysler'),
	(10, 'Daewoo'),
	(11, 'Citroen');
/*!40000 ALTER TABLE `marks` ENABLE KEYS */;

-- Dumping structure for table db_messenger.models
CREATE TABLE IF NOT EXISTS `models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mark_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_models_marks` (`mark_id`),
  CONSTRAINT `FK_models_marks` FOREIGN KEY (`mark_id`) REFERENCES `marks` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Dumping data for table db_messenger.models: ~15 rows (approximately)
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
REPLACE INTO `models` (`id`, `mark_id`, `name`) VALUES
	(7, 1, 'Golf'),
	(8, 2, 'Octavia'),
	(9, 3, 'RAV4'),
	(10, 4, 'Civic'),
	(11, 5, '208'),
	(12, 6, 'Tucson'),
	(13, 5, '308'),
	(14, 11, 'C4'),
	(16, 4, 'Pilot'),
	(17, 1, 'Polo'),
	(18, 4, 'Accord'),
	(20, 10, 'Lanos'),
	(21, 3, 'Camry'),
	(22, 8, '300c'),
	(23, 7, 'Journey'),
	(24, 7, 'Charger');
/*!40000 ALTER TABLE `models` ENABLE KEYS */;

-- Dumping structure for table db_messenger.notifications
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reg_number` varchar(10) NOT NULL,
  `status` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sender` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table db_messenger.notifications: ~0 rows (approximately)
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
REPLACE INTO `notifications` (`id`, `reg_number`, `status`, `date`, `sender`) VALUES
	(1, '12345SX', 2, '2020-09-28 01:45:47', 0);
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;

-- Dumping structure for table db_messenger.statuses
CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table db_messenger.statuses: ~5 rows (approximately)
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
REPLACE INTO `statuses` (`id`, `name`) VALUES
	(1, 'На выдачу'),
	(2, 'Резерв'),
	(3, 'В работе'),
	(4, 'Ремонт'),
	(5, 'Угон');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;

-- Dumping structure for table db_messenger.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- Dumping data for table db_messenger.users: ~7 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `login`, `password`, `email`, `group_id`) VALUES
	(1, 'test44', 'Test_444', 'test@test.com', 6),
	(5, 'Wild44', 'Wild_444', 'willy@gmail.com', 3),
	(11, 'Cowboy5', 'Cowboy_5', 'sernewday@gmail.com', 5),
	(12, 'Royals', 'Royal_44', 'roy@green.com', 3),
	(18, 'Glebov', 'Glebov', 'gleb@green.com', 4),
	(26, 'Timaty', 'Timaty_6', 'tyni@tyn.com', 5),
	(28, 'Superadmin1', 'Superadmin_1', 'admin@admin.com', 10);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table db_messenger.user_access
CREATE TABLE IF NOT EXISTS `user_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `right_object` varchar(50) NOT NULL DEFAULT '',
  `right_type` varchar(50) NOT NULL DEFAULT '',
  `right_options_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table db_messenger.user_access: ~6 rows (approximately)
/*!40000 ALTER TABLE `user_access` DISABLE KEYS */;
REPLACE INTO `user_access` (`id`, `user_id`, `right_object`, `right_type`, `right_options_id`) VALUES
	(40, 28, 'statuses', 'alert', 1),
	(50, 18, 'statuses', 'alert', 1),
	(51, 18, 'statuses', 'alert', 2),
	(52, 11, 'statuses', 'alert', 1),
	(53, 11, 'statuses', 'alert', 2),
	(54, 11, 'statuses', 'alert', 3);
/*!40000 ALTER TABLE `user_access` ENABLE KEYS */;

-- Dumping structure for trigger db_messenger.cars_after_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `cars_after_update` AFTER UPDATE ON `cars` FOR EACH ROW BEGIN
IF NEW.status_id<>OLD.status_id THEN

    INSERT INTO alerts (`sender`,`header`,`body`,`to_whom_id`)
    SELECT 
    	(SELECT login from users WHERE id=NEW.last_editor), 
        CONCAT('TC ',NEW.reg_number), 
        CONCAT('Статус изменился на ', (SELECT name from statuses WHERE statuses.id=NEW.status_id)), 
        `users`.`id` 
    FROM users WHERE 
   (
    	(SELECT COUNT(*) FROM user_access as ua WHERE ua.user_id=users.id AND ua.right_options_id=NEW.status_id AND ua.right_object='statuses' AND ua.right_type='alert')=1 
        OR
		(
            (SELECT COUNT(*) FROM user_access as ac WHERE ac.user_id=users.id AND ac.right_object='statuses' AND ac.right_type='alert')=0 AND 
            (SELECT COUNT(*) FROM group_access as ga WHERE ga.group_id=users.group_id AND ga.right_options_id=NEW.status_id AND ga.right_object='statuses' AND ga.right_type='alert')=1
        )
    );

END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
