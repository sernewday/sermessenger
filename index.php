<?php
session_start();
include 'autoload.php';

Route::add('/',function(){
    include('templ/index.php');
});

Route::add('/login',function(){
    include('templ/login.php');
});

Route::add('/logout',function(){
    include('logout.php');
});

Route::add('/userProfile',function(){
    include('userProfile.php');
});

Route::add('/users',function(){
    include('users.php');
});

Route::add('/groups',function(){
    include('groups.php');
});

Route::add('/cars',function(){
    include('cars.php');
});

Route::add('/marks',function(){
    include('marks.php');
});

Route::add('/models',function(){
    include('models.php');
});

Route::add('/statuses',function(){
    include('statuses.php');
});

Route::add('/accesses',function(){
    include('accesses.php');
});

Route::add('/alerts',function(){
    include('alerts.php');
});

Route::pathNotFound(function($param){
    include('templ/404.php');
});


Route::methodNotAllowed(function(){
    include('templ/denied.php');
});

Route::add('/dataError',function(){
    include('templ/dataError.php');
});


Route::run('/');