<?php
session_start();
include 'autoload.php';
$currentUrl = '/cars'; 
if (!isset($_SESSION['logon'])) {
	header( 'Location: /login', true, 303 ); 
    die;
    }

$isExist = new IsExistsValidator();

$Cars = new Cars();
$rows = $Cars->usconnect();
$count = round(count($rows)/17 + 0.45);


$Marks = new Marks();
$selectMarks = $Marks->hasChilds();

$Models = new Models();
$selectModels = $Models->usconnect();

$Statuses = new Statuses();
$selectStatuses = $Statuses->usconnect();

if (!empty($_GET['mkid'])) {
    if ($data = $Marks->getChildsArray($_GET['mkid'])) {
        echo json_encode($data); die;
}
}


if (!empty($_GET['did'])) {
    $data = (object) null;
    $data->id = $_GET['did'];
    $data->query_type = 'del';
    if ($Cars->save($data)) {
        echo 'OK'; die;
    } else {
        header( 'Location: /dataError?info=Record not deleted!', true, 303 );  
    }
}

if (!empty($_GET['sid'])) {
    if ($data = $Cars->getById($_GET['sid'])) {
        echo json_encode($data); die;
    } else {
        header( 'Location: /dataError?info=Select do not received!', true, 303 );  
    }
}

if (!empty($_GET['reg_number'])) {
    $data = $isExist->validate($_GET['reg_number'], 'cars', 'reg_number');
    if ($data) {
        echo json_encode($data);
        die;
    } else {
        header('Location: /dataError?info=Select do not received!', true, 303);
    }
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'add') {
    $data = $Cars->before_save($_POST); 
    if ($Cars->save($data)) {
        header( 'Location: /cars', true, 303 );
    } else {
        header( 'Location: /dataError?info=Record not insert!', true, 303 ); 
    }
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'edit') {
    $data = $Cars->before_save($_POST); 
    if ($Cars->save($data)) {
        header( 'Location: /cars', true, 303 );
    } else {
        header( 'Location: /dataError?info=Record not update!', true, 303 ); 
    }
}
include('templ/cars/index.php');
