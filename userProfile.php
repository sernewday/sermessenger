<?php
session_start();
include 'autoload.php';
$currentUrl = '/userProfile';

if (!isset($_SESSION['logon'])) {
    header('Location: /login', true, 303);
    die;
}

$Users = new Users();
$user = $Users->getById($_SESSION['id']);

$Groups = new Groups();
$select = $Groups->usconnect();

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'edit') {
    $data = $Users->before_save($_POST);
    if ($Users->save($data)) {
        header('Location: /userProfile', true, 303);
    } else {
        header('Location: /dataError?info=Record not update!', true, 303);
    }
}
include('templ/users/userProfile.php');
