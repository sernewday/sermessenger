<?php
session_start();
include 'autoload.php';
$currentUrl = '/groups';

if (!isset($_SESSION['logon'])) {
    header('Location: /login', true, 303);
    die;
}

$isExist = new IsExistsValidator();
$Groups = new Groups();
$rows = $Groups->usconnect();
$count = round(count($rows) / 17 + 0.45);

if (!empty($_GET['did'])) {
    $data = (object) null;
    $data->id = $_GET['did'];
    $data->query_type = 'del';
    if ($Groups->save($data)) {
        echo 'OK';
        die;
    } else {
        header('Location: /dataError?info=Record not deleted!', true, 303);
    }
}

if (!empty($_GET['sid'])) {
    if ($data = $Groups->getById($_GET['sid'])) {
        echo json_encode($data);
        die;
    } else {
        header('Location: /dataError?info=Select do not received!', true, 303);
    }
}

if (!empty($_GET['name'])) {
    $data = $isExist->validate($_GET['name'], 'groups', 'name'); 
    if ($data) {
        echo json_encode($data);
        die;
    } else {
        header('Location: /dataError?info=Select do not received!', true, 303);
    }
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'add') {
    $data = $isExist->validate($_POST['name'], 'groups', 'name'); 
    if ($data == 'YES') { 
    } else {
        $data = $Groups->before_save($_POST);
        if ($Groups->save($data)) {
            header('Location: /groups', true, 303);
        } else {
            header('Location: /dataError?info=Record not insert!', true, 303);
        }
    }
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'edit') {
    $data = $isExist->validate($_POST['name'], 'groups', 'name');
    if ($data == 'YES') { 
    } else {
    $data = $Groups->before_save($_POST);
    if ($Groups->save($data)) {
        header('Location: /groups', true, 303);
    } else {
        header('Location: /dataError?info=Record not update!', true, 303);
    }
}
}
include('templ/groups/index.php');
