Quick start guide
1. Make clon in root web dir.
2. Rename config-example.php to config.php
3. Change 
define('SQL_SERVER',    'localhost'); 
define('SQL_USER',      'root');
define('SQL_PWD',       'root');
define('SQL_DB_NAME',   'db_messenger');
to your real params
4. Create DB from /db/db_messenger.sql
5. Enjoy
p.s 
MySql 5.6 and higher
PHP 5.6 and higher


