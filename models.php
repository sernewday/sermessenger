<?php
session_start();
include 'autoload.php';
$currentUrl = '/models';

if (!isset($_SESSION['logon'])) {
    header('Location: /login', true, 303);
    die;
}

$isExist = new IsExistsValidator();
$Models = new Models();
$rows = $Models->usconnect();
$count = round(count($rows) / 17 + 0.45);

$Marks = new Marks();
$select = $Marks->usconnect();

if (!empty($_GET['did'])) {
    $data = (object) null;
    $data->id = $_GET['did'];
    $data->query_type = 'del';
    if ($Models->save($data)) {
        echo 'OK';
        die;
    } else {
        header('Location: /dataError?info=Record not deleted!', true, 303);
    }
}

if (!empty($_GET['sid'])) {
    if ($data = $Models->getById($_GET['sid'])) {
        echo json_encode($data);
        die;
    } else {
        header('Location: /dataError?info=Select do not received!', true, 303);
    }
}

if (!empty($_GET['name'])) {
    $data = $isExist->validate($_GET['name'], 'models', 'name');
    if ($data) {
        echo json_encode($data);
        die;
    } else {
        header('Location: /dataError?info=Select do not received!', true, 303);
    }
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'add') {
    $data = $isExist->validate($_POST['name'], 'models', 'name');
    if ($data == 'YES') {
    } else {
    $data = $Models->before_save($_POST);
    if ($Models->save($data)) {
        header('Location: /models', true, 303);
    } else {
        header('Location: /dataError?info=Record not insert!', true, 303);
    }
}
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'edit') {
    $data = $isExist->validate($_POST['name'], 'models', 'name');
    if ($data == 'YES') {
    } else {
    $data = $Models->before_save($_POST);
    if ($Models->save($data)) {
        header('Location: /models', true, 303);
    } else {
        header('Location: /dataError?info=Record not update!', true, 303);
    }
}
}
include('templ/models/index.php');
