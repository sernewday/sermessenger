<?php
session_start();
$currentUrl = '/';

if (!isset($_SESSION['logon'])) {
    header('Location: /login', true, 303);
    die;
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Ser Messenger - Main</title>
    <meta name="description" content="Ser Messenger">
    <meta name="author" content="Serhii Aksonov">
</head>

<body>
    <?php
    include "templ/header.php";
    include "templ/menu.php";
    ?>

    <div class="page_404">
        Please be sure to check your spam box in case you haven't found any content here.
    </div>
</body>

</html>