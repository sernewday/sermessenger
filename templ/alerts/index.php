<head>
    <meta charset="utf-8">
    <title>Ser Messenger - Alerts</title>
</head>

<body>
    <?php
    include "templ/header.php";
    include "templ/menu.php";
    ?>
    <div class="alerts">
        <table class="alerts">
            <thead>
                <tr>
                    <th class="alerts-th">ID</th>
                    <th class="alerts-th">Date</th>
                    <th class="alerts-th">Sender</th>
                    <th class="alerts-th">Header</th>
                    <th class="alerts-th">Body</th>
                    <th class="alerts-th">
                        <a href="javascript: showAllAlert();"><img id="img_alerts" src="/images/add.png" title="Show all"></a>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($rows as $row) {
                    echo ($row['read'] == 0 ? '<tr class="alert-read">' : '<tr class="alert-unread" style="display: none;">');
                    echo '<td class="alerts-td1">' . $row['id'] . '</td>';
                    echo  '<td class="alerts-td">' . date('d.m.y H:i:s', strtotime($row['date'])) . '</td>';
                    echo  '<td class="alerts-td">' . $row['sender'] . '</td>';
                    echo  '<td class="alerts-td">' . $row['header'] . '</td>';
                    echo  '<td class="alerts-td">' . $row['body'] . '</td>';
                    if ($row['read'] == 0) {
                    echo  '<td class="alerts-td2"><a href=""><img src="/images/non-active-star.png"></a></td>';
                    } else {
                    echo  '<td class="alerts-td2"><a href=""><img src="/images/active-star.png"></td>';
                    }
                }
                ?>
                <tr>
                    <th class="alerts-th">
                        << pre</th> <th class="alerts-th" colspan="4">page 1 of <?= $count ?>
                    </th>
                    <th class="alerts-th">next >></th>
                </tr>
            </tbody>
        </table>
    </div>

</body>