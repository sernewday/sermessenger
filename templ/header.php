<link href="images/favicon.png" rel="shortcut icon" />
<script src="js/script.js"></script>
<link rel="stylesheet" href="css/site.css">
<div class="header">
    <h1>SER MESSENGER</h1>
</div>
<?php $Alerts = new Alerts();
$alerts = $Alerts->calcAlerts();
$_SESSION['alerts'] = $alerts['count_alerts'];
?>
<div id="notification_wrapper" class="notification_wrapper">
    <div id="notification_drawer" class="notification_drawer"></div>
</div>
<div id="message_modal" class="message_modal">
    <div id="validate_massage" class="validate_massage">
    </div>
</div>
