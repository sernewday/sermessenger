<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Ser Messenger - Login</title>
    <meta name="description" content="Ser Messenger">
    <meta name="author" content="Serhii Aksonov">
</head>

<body>
    <?php 
        include "templ/headerLogin.php";
    ?>
    <div class="login">
        <h1>Login</h1>
        <form action="login.php" method="post">
            <label for="username">
                Login >
            </label>
            <?php
            if (!empty($_GET['e']) && $_GET['e'] == 'l') {
                echo '<input type="text" name="login" placeholder="Bad Login - Try Again!!!" id="login" required>';
            } else {
                echo '<input type="text" name="login" placeholder="Username" id="login" required>';
            }
            ?>
            <label for="password">
                Password >
            </label>
            <?php
            if (!empty($_GET['e']) && $_GET['e'] == 'p') {
                echo '<input type="text" name="password" placeholder="Bad Password - Try Again!!!" id="password" required>';
            } else {
                echo '<input type="password" name="password" placeholder="Password" id="password" required>';
            }
            ?>
            <input type="submit" value="E n t e r">
        </form>
    </div>
</body>

</html>