<div class="menu">
<nav>
<a <?php echo ($currentUrl == '/' ? 'class="a-menu-active"' : 'class="a-menu"'); ?> href="/">| Main |</a>
<a <?php echo ($currentUrl == '/userProfile' ? 'class="a-menu-active"' : 'class="a-menu"'); ?> href="/userProfile">| Profile |</a>
<a <?php echo ($currentUrl == '/users' ? 'class="a-menu-active"' : 'class="a-menu"'); ?> href="/users">| Users |</a>
<a <?php echo ($currentUrl == '/groups' ? 'class="a-menu-active"' : 'class="a-menu"'); ?> href="/groups">| Groups |</a>
<a <?php echo ($currentUrl == '/cars' ? 'class="a-menu-active"' : 'class="a-menu"'); ?> href="/cars">| Cars |</a>
<a <?php echo ($currentUrl == '/marks' ? 'class="a-menu-active"' : 'class="a-menu"'); ?> href="/marks">| Marks |</a>
<a <?php echo ($currentUrl == '/models' ? 'class="a-menu-active"' : 'class="a-menu"'); ?> href="/models">| Models |</a>
<a <?php echo ($currentUrl == '/statuses' ? 'class="a-menu-active"' : 'class="a-menu"'); ?> href="/statuses">| Statuses |</a>
<a <?php echo ($currentUrl == '/accesses' ? 'class="a-menu-active"' : 'class="a-menu"'); ?> href="/accesses">| Access |</a>
<a class="a-menu" href="/logout">| Logout <span class="color_span_menu"><?= ' '.$_SESSION['login'].' ' ?></span> |</a>
<a <?php echo ($currentUrl == '/alerts' ? 'class="a-menu-alerts-active"' : 'class="a-menu-alerts"'); ?> title="You have <?= $_SESSION['alerts']?> alerts" href="/alerts">(<img src="images/ring.png"> <?= $_SESSION['alerts']?>)</a>
</nav>
</div>
