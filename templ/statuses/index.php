<head>
    <meta charset="utf-8">
    <title>Ser Messenger - Statuses</title>
</head>

<body>
    <?php
    include "templ/header.php";
    include "templ/menu.php";
    ?>
    <div id="model_form" class="statuses_modal">
        <a href="javascript: clearForm(); closeForm('statuses')" title="close">
            <img class="close" align="right" src="/images/close-white.png">
            <h1>Statuses Card</h1>
        </a>
        <form action="statuses.php" onsubmit="ExistName(this, 1, 2, 3);return false;" method="post">
            <label for="id_disabled">
                Id >
            </label>
            <input disabled="disabled" type="text" name="id_disabled" placeholder="ID" id="id_disabled" value="0" required>
            <label for="name">
                Status >
            </label>
            <input type="text" name="name" placeholder="Like [a-Z,а-Я,_,0-9] not less 3" id="name" required pattern="<?= REG_NAME ?>" title="Like [a-Z,а-Я,_,0-9] not less 3">
            <input type="hidden" id="id" name="id" value="0">
            <input type="hidden" id="query_type" name="query_type" value="">
            <input type="submit" value="E n t e r">
        </form>
    </div>
    <div class="statuses">
        <table class="statuses">
            <tbody>
                <tr>
                    <th class="statuses-th">ID</th>
                    <th class="statuses-th">Name</th>
                    <th class="statuses-th img-icon">
                        <a href="javascript: showStatusForm();"><img src="images/add.png" title="Add"></a>
                        <a class="a-icon" href="#"><img src="images/edit.png" title="Edit"></a>
                        <a class="a-icon" href="#"><img src="images/trash.png" title="Trash"></a>
                    </th>
                </tr>
                <?php foreach ($rows as $row) {
                    echo '<tr><td class="statuses-td1">' . $row['id'] . '</td>';
                    echo  '<td class="statuses-td">' . $row['name'] . '</td>';
                    echo  '<td class="statuses-td2 img-icon">';
                    echo '<a href="javascript: showStatusForm();"><img src="images/add.png" title="Add"></a>';
                    echo '<a class="a-icon" href="javascript: showStatusForm(' . $row['id'] . ');"><img src="images/edit.png" title="Edit"></a>';
                    echo '<a class="a-icon" href="javascript: alert(`YOU DELETED RECORD ' . $row['id'] . '`); getXHR(`/statuses?did=' . $row['id'] . '`,`/statuses`);"><img src="images/trash.png" title="Trash"></a></td></tr>';
                }
                ?>
                <tr>
                    <th class="statuses-th">
                        << pre</th> <th class="statuses-th">page 1 of <?= $count ?>
                    </th>
                    <th class="statuses-th">next >></th>
                </tr>
            </tbody>
        </table>
    </div>

</body>