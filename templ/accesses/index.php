<?php
session_start();
if (!isset($_SESSION['logon'])) {
    header('Location: /login', true, 303);
    die;
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Ser Messenger - Accesses</title>
    <meta name="description" content="Ser Messenger">
    <meta name="author" content="Serhii Aksonov">
</head>

<body>
    <?php
    include "templ/header.php";
    include "templ/menu.php";
    ?>
    <form action="accesses.php" onsubmit="checkboxValidate(this); return false;" method="post">
        <div class="accesses">
            <div class="accesses-block-left">
                <div class="div-top">
                    <table>
                        <thead>
                            <tr>
                                <th>Groups & Users</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($whomTree as $group => $user) {
                                for ($i = 0; $i < count($user); $i++) {
                                    if ($i == 0) {
                                        echo '<tr>';
                                        echo '<td><button type="button">
                            <label><input type="radio"  onclick="showGroupRights(' . $group . ');" id="group_id.' . $group . '" name="owner_id" value="group_id.' . $group . '"/>' . $user[$i] . '</label></button></td>';
                                        echo     '</tr>';
                                    } else {
                                        echo '<tr>';
                                        echo '<td><label><input type="radio" onclick="showUserRights(' . $user[$i][0] . ');" id="user_id.' . $user[$i][0] . '" name="owner_id" value="user_id.' . $user[$i][0] . '"/>' . $user[$i][1] . '</label></td>';
                                        echo     '</tr>';
                                    }
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="div-bottom">
                    <table>
                        <thead>
                            <tr>
                                <th>Rights objects & types</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><button type="button">
                                        <label><input type="radio" name="right_object" value="statuses" />Alerts of:</label></button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label><input type="radio" name="right_type" value="alert" />status change</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="accesses-body">
                <div class="accesses-block-header">
                    Alert
                </div>
                <div class="accesses-block-body">
                    <span>Уважаемые пользователи группы Gold! <br/><br/>
                        Вам предоставлено право на отслеживание изменения статуса транспортных средтсв. <br/>
                        На данный момент вам доступны следующие статусы: "На выдачу", "В работе". <br/><br/>
                        С уважением, <br/>
                        Администрация.
                    </span>
                </div>
            </div>
            <div class="accesses-block">
                <div>
                    <table>
                        <thead>
                            <tr>
                                <th>Rights options</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($alertOptions as $options) {
                                echo '<tr>';
                                echo '<td><label><input type="checkbox"  name="right_options_id[]" value="' . $options['id'] . '"/>' . $options['name'] . '</label></td>';
                                echo     '</tr>';
                            }
                            ?>
                            <tr>
                                <td><button type="submit" class="button-submit">Save</td>
                            </tr>
                            <tr>
                                <td><button type="button" class="button-button" onclick="clearForm()">Clear</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <img src="/images/banner.jpg" alt="banner">
                </div>
            </div>
        </div>

    </form>
</body>

</html>