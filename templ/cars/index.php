<head>
    <meta charset="utf-8">
    <title>Ser Messenger - Cars</title>
</head>

<body>
    <?php
    include "templ/header.php";
    include "templ/menu.php";
    ?>
    <div id="car_form" class="cars_modal">
        <a href="javascript: clearForm(); closeForm('cars')" title="close">
            <img class="close" align="right" src="/images/close-white.png">
            <h1>Cars Card</h1>
        </a>
        <form action="cars.php"  onsubmit="ExistName(this, 3, 6, 7);return false;" method="post">
            <label for="id_disabled">
                Id >
            </label>
            <input disabled="disabled" type="text" name="id_disabled" placeholder="ID" id="id_disabled" value="0" required>
            <label for="mark_id">
                Mark >
            </label>
            <select onchange="onchangeMarkInCarForm(this.value)" id="mark_id" name="mark_id" required>
                <?php foreach ($selectMarks as $option) {
                    echo '<option id="opmk_' . $option['id'] . '" value="' . $option['id'] . '">' . $option['name'] . '</option>';
                }
                ?>
            </select>
            <label for="model_id">
                Model >
            </label>
            <select id="model_id" name="model_id" required>
                <?php foreach ($selectModels as $option) {
                    echo '<option id="opmd_' . $option['id'] . '" value="' . $option['id'] . '">' . $option['name'] . '</option>';
                }
                ?>
            </select>
            <label for="reg_number">
                Reg. Number >
            </label>
            <input type="text" name="reg_number" placeholder="Like [A-Z,0-9] not less 7" id="reg_number" required pattern="<?= REG_NAME ?>" title="Like [A-Z,0-9] not less 7">
            <label for="rent_amount">
                Rent Amount >
            </label>
            <input type="text" name="rent_amount" placeholder="Not required, Like 12345.67, Not negative" id="rent_amount" pattern="<?= REG_DECIMAL ?>" title="Not required, Like 12345.67, Not negative">
            <label for="status_id">
                Status >
            </label>
            <select id="status_id" name="status_id" required>
                <?php foreach ($selectStatuses as $option) {
                    echo '<option id="ops_' . $option['id'] . '" value="' . $option['id'] . '">' . $option['name'] . '</option>';
                }
                ?>
            </select>
            <input type="hidden" id="id" name="id" value="0">
            <input type="hidden" id="query_type" name="query_type" value="">
            <input type="hidden" name="last_editor" value="<?= $_SESSION['id'] ?>">
            <input type="submit" value="E n t e r">
        </form>
    </div>
    <div class="cars">
        <table class="cars">
            <tbody>
                <tr>
                    <th class="cars-th">ID</th>
                    <th class="cars-th">Mark</th>
                    <th class="cars-th">Model</th>
                    <th class="cars-th">Reg. Number</th>
                    <th class="cars-th">Rent Amount</th>
                    <th class="cars-th">Status</th>
                    <th class="cars-th img-icon">
                        <a href="javascript: showCarForm();"><img src="images/add.png" title="Add"></a>
                        <a class="a-icon" href="#"><img src="images/edit.png" title="Edit"></a>
                        <a class="a-icon" href="#"><img src="images/trash.png" title="Trash"></a>
                    </th>
                </tr>
                <?php foreach ($rows as $row) {
                    echo '<tr><td class="cars-td1">' . $row['id'] . '</td>';
                    echo  '<td class="cars-td">' . $row['markname'] . '</td>';
                    echo  '<td class="cars-td">' . $row['modelname'] . '</td>';
                    echo  '<td class="cars-td">' . $row['reg_number'] . '</td>';
                    echo  '<td class="cars-td">' . $row['rent_amount'] . '</td>';
                    echo  '<td class="cars-td">' . $row['statusname'] . '</td>';
                    echo  '<td class="cars-td2 img-icon">';
                    echo '<a href="javascript: showCarForm();"><img src="images/add.png" title="Add"></a>';
                    echo '<a class="a-icon" href="javascript: showCarForm(' . $row['id'] . ');"><img src="images/edit.png" title="Edit"></a>';
                    echo '<a class="a-icon" href="javascript: alert(`YOU DELETED RECORD ' . $row['id'] . '`); getXHR(`/cars?did=' . $row['id'] . '`,`/cars`);"><img src="images/trash.png" title="Trash"></a></td></tr>';
                }
                ?>
                <tr>
                    <th class="cars-th">
                        << pre</th> <th class="cars-th" colspan="5">page 1 of <?= $count ?>
                    </th>
                    <th class="cars-th">next >></th>
                </tr>
            </tbody>
        </table>
    </div>

</body>