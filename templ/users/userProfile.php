<head>
    <meta charset="utf-8">
    <title>Ser Messenger - User Profile</title>
</head>
<body>
    <?php
    include "templ/header.php";
    include "templ/menu.php";
    ?>
    <div id="userProfile_form" class="userProfile_modal">
        <h1>User Profile</h1>
        <form action="userProfile.php" method="post">
            <label for="disabled">
                Id >
            </label>
            <input disabled="disabled" type="text" name="disabled" placeholder="ID" id="disabled" value="<?= $user['id'] ?>">
            <label for="login">
                Login >
            </label>
            <?php
            if (!empty($_GET['e']) && $_GET['e'] == 'n') {
                echo '<input type="text" name="login" placeholder="Bad login - Enter Like [a-Z,0-9,_] not less 6" id="login" value="" autocomplete="login" required pattern="'.REG_LOGIN.'" title="Like [a-Z,0-9,_] not less 6">';
            } else {
                echo '<input type="text" name="login" placeholder="Like [a-Z,0-9,_] not less 6" id="login" value="' . $user['login'] . '" autocomplete="login" required pattern="'.REG_LOGIN.'" title="Like [a-Z,0-9,_] not less 6">';
            }
            ?>
            <label for="password">
            <input onclick="eyePass()"  type="checkbox" id="eye_pass" title="Show password"> </input> Password > 
            </label>
            <?php
            if (!empty($_GET['e']) && $_GET['e'] == 'p') {
                echo '<input type="text" name="password" placeholder="Bad Password - Required 1 or more [a-z,A_Z,0-9,_] not less 8" id="password" value="" autocomplete="current-password" required pattern="'.REG_PASSWORD.'" title="Required 1 or more [a-z,A_Z,0-9,_] not less 8">';
            } else {
                echo '<input type="password" name="password" placeholder="Required 1 or more [a-z,A_Z,0-9,_] not less 8" id="password" value="' . $user['password'] . '" autocomplete="current-password" required pattern="'.REG_PASSWORD.'" title="Required 1 or more [a-z,A_Z,0-9,_] not less 8">';
            }
            ?>
            <label for="email">
                Mail >
            </label>
            <?php
            if (!empty($_GET['e']) && $_GET['e'] == 'e') {
                echo '<input type="text" name="email" placeholder="Bad E-Mail - Enter Like nik@domain.com" id="email" value="" required pattern="'.REG_EMAIL.'" title="nik@domain.com">';
            } else {
                echo '<input type="text" name="email" placeholder="Like nik@domain.com" id="email" value="' . $user['email'] . '" required pattern="'.REG_EMAIL.'" title="nik@domain.com">';
            }
            ?>
            <label for="group_id">
                Group >
            </label>
            <select id="group_id" name="group_id">
                <?php foreach ($select as $option) {
                    if ($user['group_id'] == $option['id']) {
                        echo '<option selected="selected" id="op_' . $option['id'] . '" value="' . $option['id'] . '">' . $option['name'] . '</option>';
                    } else {
                    echo '<option id="op_' . $option['id'] . '" value="' . $option['id'] . '">' . $option['name'] . '</option>';
                    }
                }
                ?>
            </select>
            <input type="hidden" id="id" name="id" value="<?= $user['id'] ?>">
            <input type="hidden" id="query_type" name="query_type" value="edit">
            <input type="submit" value="E n t e r">
        </form>
    </div>

</body>