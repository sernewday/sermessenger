<head>
    <meta charset="utf-8">
    <title>Ser Messenger - Users</title>
</head>

<body>
    <?php
    include "templ/header.php";
    include "templ/menu.php";
    ?>
    <div id="user_form" class="users_modal">
        <a href="javascript: clearForm(); closeForm('users')" title="close">
            <img class="close" align="right" src="/images/close-white.png">
            <h1>Users Card</h1>
        </a>
        <form action="users.php" onsubmit="ExistName(this, 1, 6, 7);return false;" method="post">
            <label for="id_disabled">
                Id >
            </label>
            <input disabled="disabled" type="text" name="id_disabled" placeholder="ID" id="id_disabled" value="0">
            <label for="login">
                Login >
            </label>
            <input type="text" name="login" placeholder="Like [a-Z,0-9,_] not less 6" id="login" value="<?= $rows['login'] ?>" autocomplete="login" required pattern="<?= REG_LOGIN ?>" title="Like [a-Z,0-9,_] not less 6">
            <label for="password">
                <input onclick="eyePass()" type="checkbox" id="eye_pass" title="Show password"> </input> Password >
            </label>
            <?php
            if (!empty($_GET['e']) && $_GET['e'] == 'p') {
                echo '<input type="text" name="password" placeholder="Bad Password - Required 1 or more [a-z,A_Z,0-9,_] not less 8" id="password" autocomplete="current-password" required pattern="' . REG_PASSWORD . '" title="Required 1 or more [a-z,A_Z,0-9,_] not less 8">';
            } else {
                echo '<input type="password" name="password" placeholder="Required 1 or more [a-z,A_Z,0-9,_] not less 8" id="password" value="' . $rows['password'] . '" autocomplete="current-password" required pattern="' . REG_PASSWORD . '" title="Required 1 or more [a-z,A_Z,0-9,_] not less 8">';
            }
            ?>
            <label for="email">
                Mail >
            </label>
            <?php
            if (!empty($_GET['e']) && $_GET['e'] == 'e') {
                echo '<input type="text" name="email" placeholder="Bad E-Mail - Enter Like nik@domain.com" id="email" required pattern="' . REG_EMAIL . '" title="nik@domain.com">';
            } else {
                echo '<input type="text" name="email" placeholder="Like nik@domain.com" id="email" value="' . $rows['email'] . '" required pattern="' . REG_EMAIL . '" title="nik@domain.com">';
            }
            ?>
            <label for="group_id">
                Group >
            </label>
            <select id="group_id" name="group_id" required>
                <?php foreach ($select as $option) {
                    echo '<option id="op_' . $option['id'] . '" value="' . $option['id'] . '">' . $option['name'] . '</option>';
                }
                ?>
            </select>
            <input type="hidden" id="id" name="id" value="0">
            <input type="hidden" id="query_type" name="query_type" value="">
            <input type="submit" value="E n t e r">
        </form>
    </div>
    <div class="users">
        <table class="users">
            <tbody>
                <tr>
                    <th class="users-th">ID</th>
                    <th class="users-th">Login</th>
                    <th class="users-th">Password</th>
                    <th class="users-th">Mail</th>
                    <th class="users-th">Group</th>
                    <th class="users-th img-icon">
                        <a href="javascript: showUserForm();"><img src="images/add.png" title="Add"></a>
                        <a class="a-icon" href="#"><img src="images/edit.png" title="Edit"></a>
                        <a class="a-icon" href="#"><img src="images/trash.png" title="Trash"></a>
                    </th>
                </tr>
                <?php foreach ($rows as $row) {
                    echo '<tr><td class="users-td1">' . $row['id'] . '</td>';
                    echo  '<td class="users-td">' . $row['login'] . '</td>';
                    $showpass = '';
                    for ($i = 0; $i < strlen($row['password']); $i++) {
                        $showpass = $showpass . '.';
                    }
                    echo  '<td class="users-td">' . $showpass . '</td>';
                    echo  '<td class="users-td">' . $row['email'] . '</td>';
                    echo  '<td class="users-td">' . $row['groupname'] . '</td>';
                    echo  '<td class="users-td2 img-icon">';
                    echo '<a href="javascript: showUserForm();"><img src="images/add.png" title="Add"></a>';
                    echo '<a class="a-icon" href="javascript: showUserForm(' . $row['id'] . ');"><img src="images/edit.png" title="Edit"></a>';
                    echo '<a class="a-icon" href="javascript: alert(`YOU DELETED RECORD ' . $row['id'] . '`); getXHR(`/users?did=' . $row['id'] . '`,`/users`);"><img src="images/trash.png" title="Trash"></a></td></tr>';
                }
                ?>
                <tr>
                    <th class="users-th">
                        << pre</th> <th class="users-th" colspan="4">page 1 of <?= $count ?>
                    </th>
                    <th class="users-th">next >></th>
                </tr>
            </tbody>
        </table>
    </div>

</body>