<head>
    <meta charset="utf-8">
    <title>Ser Messenger - Models</title>
</head>

<body>
    <?php
    include "templ/header.php";
    include "templ/menu.php";
    ?>
    <div id="model_form" class="models_modal">
        <a href="javascript: clearForm(); closeForm('models')" title="close">
            <img class="close" align="right" src="/images/close-white.png">
            <h1>Models Card</h1>
        </a>
        <form action="models.php" onsubmit="ExistName(this, 2, 3, 4);return false;" method="post">
            <label for="id_disabled">
                Id >
            </label>
            <input disabled="disabled" type="text" name="id_disabled" placeholder="ID" id="id_disabled" value="0" required>
            <label for="mark_id">Mark</label>
            <select id="mark_id" name="mark_id" required>
                <?php foreach ($select as $option) {
                    echo '<option id="op_' . $option['id'] . '" value="' . $option['id'] . '">' . $option['name'] . '</option>';
                }
                ?>
            </select>
            <label for="name">
                Model >
            </label>
            <input type="text" name="name" placeholder="Like [a-Z,а-Я,_,0-9]" id="name" required pattern="<?= REG_NAME ?>" title="Like [a-Z,а-Я,_,0-9] not less 3">
            <input type="hidden" id="id" name="id" value="0">
            <input type="hidden" id="query_type" name="query_type" value="">
            <input type="submit" value="E n t e r">
        </form>
    </div>
    <div class="models">
        <table class="models">
            <tbody>
                <tr>
                    <th class="models-th">ID</th>
                    <th class="models-th">Mark</th>
                    <th class="models-th">Model Name</th>
                    <th class="models-th img-icon">
                        <a href="javascript: showModelForm();"><img src="images/add.png" title="Add"></a>
                        <a class="a-icon" href="#"><img src="images/edit.png" title="Edit"></a>
                        <a class="a-icon" href="#"><img src="images/trash.png" title="Trash"></a>
                    </th>
                </tr>
                <?php foreach ($rows as $row) {
                    echo '<tr><td class="models-td1">' . $row['id'] . '</td>';
                    echo '<td class="models-td">' . $row['markname'] . '</td>';
                    echo '<td class="models-td">' . $row['name'] . '</td>';
                    echo '<td class="models-td2 img-icon">';
                    echo '<a href="javascript: showModelForm();"><img src="images/add.png" title="Add"></a>';
                    echo '<a class="a-icon" href="javascript: showModelForm(' . $row['id'] . ');"><img src="images/edit.png" title="Edit"></a>';
                    echo '<a class="a-icon" href="javascript: alert(`YOU DELETED RECORD ' . $row['id'] . '`); getXHR(`/models?did=' . $row['id'] . '`,`/models`);"><img src="images/trash.png" title="Trash"></a></td></tr>';
                }
                ?>
                <tr>
                    <th class="models-th">
                        << pre</th> <th class="models-th" colspan="2">page 1 of <?= $count ?>
                    </th>
                    <th class="models-th">next >></th>
                </tr>
            </tbody>
        </table>
    </div>

</body>