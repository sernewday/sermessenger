<?php
session_start();
include 'autoload.php';
$currentUrl = '/login';

$Login = new Login();

if ($User = $Login->usconnect()) {

        if ($_POST['password'] == $User['password']) {
            session_regenerate_id();
            $_SESSION['logon'] = TRUE;
            $_SESSION['login'] = $_POST['login'];
            $_SESSION['id'] = $User['id'];

            header( 'Location: /');
        } else {
            header( 'Location: /login?e=p', true, 303 );
        }
    }


